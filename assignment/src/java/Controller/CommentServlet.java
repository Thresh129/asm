/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Controller;

import DataAccess.CommentFacadeLocal;
import Entity.Comment;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;
import javax.ejb.EJB;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Admin
 */
public class CommentServlet extends HttpServlet {

    @EJB
    private CommentFacadeLocal commentFacade;
    
    List<Comment> list;
    
    boolean newComment;

    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CommentServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CommentServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        
        String postIdString = request.getParameter("postId");
        int postIdInteger = Integer.parseInt(postIdString);
        
        if (action.equals("VIEW")) {
            
            String postUsername = request.getParameter("postUsername");
            String postContent = request.getParameter("postContent");
            String postCreateAt = request.getParameter("postCreateAt");

            list = commentFacade.listComment(postIdInteger);

            request.setAttribute("postId", postIdInteger);
            request.setAttribute("postUsername", postUsername);
            request.setAttribute("postContent", postContent);
            request.setAttribute("postCreateAt", postCreateAt);

            request.setAttribute("getAllComment", list);

            request.getRequestDispatcher("comment.jsp").forward(request, response);
        } else if (action.equals("ADD")) {
            
            String username = request.getParameter("username");
            String content = request.getParameter("content");
            
            newComment = commentFacade.addComment(username, postIdInteger, content);
            
            request.getRequestDispatcher("comment.jsp").forward(request, response);
        }
    }

    @Override
    public String getServletInfo() {
        return "Short description";
    }

}

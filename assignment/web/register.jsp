<%-- 
    Document   : register
    Created on : Jan 8, 2020, 9:56:24 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register Page</title>
        <link href="css/style.css" type="text/css" rel="stylesheet"/>
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    </head>
    <body>
        <div class="container"> 
            <div  class="row margin-top-50">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <p class="first-title">Account Register</p>
                    <form action="RegisterServlet" method="POST">
                        <input class="input-user" type="text" name="username" placeholder="Username"/><br/>
                        <input class="input-pass" type="password" name="password" placeholder="Password"><br/>
                        <input class="login-btn" type="submit" value="SIGN UP"/>
                    </form>
                    <div class="last-title-1">
                        Already have an account? <a href="login.jsp">Back</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

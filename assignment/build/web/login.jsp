<%-- 
    Document   : login
    Created on : Jan 8, 2020, 9:55:54 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Login Page</title>
        <link href="css/style.css" type="text/css" rel="stylesheet"/>
        <link href="css/bootstrap.min.css" type="text/css" rel="stylesheet"/>
    </head>
    <body>
        <div class="container">            
            <div  class="row margin-top-50">
                <div class="col-md-4"></div>
                <div class="col-md-4">
                    <p class="first-title">Account Login</p>
                    <form action="LoginServlet" method="POST">
                        <input class="input-user" type="text" name="username" placeholder="Username"/><br/>
                        <input class="input-pass" type="password" name="password" placeholder="Password"><br/>
                        <input class="login-btn" type="submit" value="SIGN IN"/>
                    </form>
                    <div class="last-title">
                        Create an account? <a href="register.jsp">Sign up</a>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

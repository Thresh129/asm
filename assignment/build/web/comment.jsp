<%-- 
    Document   : comment
    Created on : Jan 8, 2020, 9:54:56 AM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<c:if test="${sessionScope.loginUsername == null}">
    <jsp:forward page="login.jsp"/>
</c:if>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Comment Page</title>
    </head>
    <body>
        <h1>Welcome, ${sessionScope.loginUsername}</h1>
        <div style="border:1px solid black;padding:10px">
            <h3 style="display:inline">${postUsername}</h3><p style="display:inline"> - ${postCreateAt}</p>
            <p>${postContent}</p>
        </div>
        <c:forEach var="comment" items="${getAllComment}">
            <div style="border:1px solid black;border-radius:15px;margin-top:10px">
                <ul style="list-style-type:none;">
                    <li>${comment.username} - ${comment.createAt} :</li>
                    <li>${comment.content}</li>
                </ul>
            </div>
        </c:forEach>
        <form action="CommentServlet" method="POST">
            <input type="hidden" name="username" value="${sessionScope.loginUsername}"/>
            <input type="hidden" name="postId" value="${postId}"/>
            <input type="text" name="content"/>
            <input type="hidden" name="action" value="ADD"/>
            <input type="submit" value="Comment"/>
        </form>
        <a href="home.jsp"><button style="margin-top:20px">Back</button></a>
    </body>
</html>
